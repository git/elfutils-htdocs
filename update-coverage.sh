#!/bin/bash

# Any error is fatal
set -e

# We take one argument, the version (e.g. 0.169)
if [ $# -ne 1 ]; then
  echo "$0 <version> (e.g. 0.169)"
  exit 1
fi

version="$1"

# Create a temporary directoy and make sure it is cleaned up.
tempdir=$(mktemp -d) || exit
trap "rm -rf -- ${tempdir}" EXIT

# Remember where we are (should be current elfutils-htdocs checkout)
# and go to the tempdir for a fresh elfutils git checkout, build and
# coverage run.

pushd "${tempdir}"

git clone git://sourceware.org/git/elfutils.git
cd elfutils
git checkout -b "$version" "elfutils-${version}"
autoreconf -v -f -i
# Note we want srcdir == builddir for better output.
${tempdir}/elfutils/configure --enable-maintainer-mode --enable-gcov
make -j$(nproc)
make check
make coverage

# Go back end get our new coverage data
popd
mv "${tempdir}/elfutils/coverage" "coverage-version/${version}"
rm "coverage"
ln -s "coverage-version/${version}" "coverage"
echo "<li><a href=\"${version}\">${version}</a></li>" >> coverage-version/index.html
git add "coverage"
git add coverage-version/index.html
git add --all "coverage-version/${version}"
git commit -m "Update coverage to elfutils-${version} using $0"

# Cleanup
trap - EXIT
exit
